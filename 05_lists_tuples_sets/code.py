l = ["Bob", "Rolf", "Anne"] # list
t = ("Bob", "Rolf", "Anne") # tuple: can't add/remove - modify elements
s = {"Bob", "Rolf", "Anne"} # set: can't have duplicate elements, order is not guaranteed

# Access individual items in lists and tuples using the index.

print(f"First element in the list is {l[0]} from {len(l)} elements")
print(t[0])
# print(s[0])  # This gives an error because sets are unordered, so accessing element 0 of something without order doesn't make sense.

# Modify individual items in lists using the index.

l[0] = "Smith"
# t[0] = "Smith"  # This gives an error because tuples are "immutable".

print(l)
print(t)

# Add to a list by using `.append`

l.append("Jen")
print(l)
# Tuples cannot be appended to because they are immutable.

# Add to sets by using `.add`

s.add("Jen")
print(s)

# Sets can't have the same element twice.

s.add("Bob")
print(s)
